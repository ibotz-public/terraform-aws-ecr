resource "aws_ecr_repository_policy" "default" {
  repository = aws_ecr_repository.default.name
  policy     = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "AllowPull",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          ${join(",", formatlist("\"arn:aws:iam::%s:root\"", var.trust_accounts))}
        ]
      },
      "Action": [
        "ecr:GetDownloadUrlForLayer",
        "ecr:BatchGetImage",
        "ecr:ListImages",
        "ecr:BatchCheckLayerAvailability"
      ]
    },
    {
      "Sid": "AllowWriteMgmt",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          ${join(",", formatlist("\"arn:aws:iam::%s:root\"", var.trust_accounts))}
        ]
      },
      "Action": [
        "ecr:GetDownloadUrlForLayer",
        "ecr:BatchGetImage",
        "ecr:BatchCheckLayerAvailability",
        "ecr:PutImage",
        "ecr:ListImages",
        "ecr:InitiateLayerUpload",
        "ecr:UploadLayerPart",
        "ecr:CompleteLayerUpload"
      ]
    }
  ]
}
EOF
}
