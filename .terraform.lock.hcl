# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version = "3.13.0"
  hashes = [
    "h1:kXU5fbn63Gj1ixR8n1Dwm256MVqXfMrSgefLC6ITt0g=",
  ]
}
